FROM node:22.1-alpine

WORKDIR /app

COPY app/package.json .

RUN npm install

COPY app .

USER node

EXPOSE 3000

CMD ["npm", "start"]