@Library('jenkins-sl')_

pipeline {
    agent any

    environment {
        IMAGE_NAME = '047527637988.dkr.ecr.us-east-1.amazonaws.com/xwindwolf/jenkins-exercise'
    }

    parameters {
        choice(name: 'UPDATE_TYPE', choices: ['patch', 'minor', 'major'], description: 'Select the type of update')
    }

    stages {
        stage("Increment Version") {
            steps {
                script {
                    env.VERSION=sh(script: "~/.nvm/versions/node/v22.1.0/bin/npm version ${params.UPDATE_TYPE} --prefix app", returnStdout: true)
                }
            }
        }
        stage("Test") {
            steps {
                script {
                    sh '~/.nvm/versions/node/v22.1.0/bin/npm test --prefix app'
                }
            }
        }
        stage("Docker Build") {
            when {
                expression {
                    currentBuild.result != 'UNSTABLE'
                }
            }

            steps {
                script {
                    buildImage("$env.IMAGE_NAME:$env.VERSION")
                }
            }
        }
        stage("Docker Push") {
            when {
                expression {
                    currentBuild.result != 'UNSTABLE'
                }
            }
            
            steps {
                script {
                    dockerLogin("$env.IMAGE_NAME")
                    pushImage("$env.IMAGE_NAME")
                }
            }
        }
        stage("Commit Update Bump") {
            when {
                expression {
                    currentBuild.result != 'UNSTABLE'
                }
            }
            
            steps {
                script {
                    withCredentials([usernamePassword(credentialsId: 'gitlab', usernameVariable: 'GITLAB_USER', passwordVariable: 'GITLAB_PASS')]) {
                        sh "git remote set-url origin https://${env.GITLAB_USER}:${env.GITLAB_PASS}@gitlab.com/xwindwolf/twn-jenkins-exercise.git"
                        sh 'git config --global user.email jenkins@example.com'
                        sh 'git config --global user.name "jenkins"'

                        sh 'git add .'
                        sh 'git commit -m "CI: Bump version"'
                        sh "git push origin HEAD:${env.BRANCH_NAME}"
                    }
                }
            }
        }
        stage("Revert Update Bump") {
            when {
                expression {
                    currentBuild.result == 'UNSTABLE'
                }
            }

            steps {
                script {
                    sh 'git restore app/package.json'
                }
            }
        }
    }
}